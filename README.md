# satisfactory

Factory planing

## Copper Factory

### MK2

- In: 120 Copper Ore
- Out:
  - 30 Copper Sheet
  - 30 Cable
  - 60 Wire

[![Copper Factory MK2](https://gitlab.com/XenGi/satisfactory/-/raw/main/Copper%20Factory%20MK2.drawio.svg)](https://app.diagrams.net/#AXenGi%2Fsatisfactory%2Fmain%2FCopper%20Factory%20MK2.drawio.svg)

### MK3

- In: 270 Copper Ore
- Out:
  - 60 Copper Sheet
  - 60 Cable
  - 180 Wire

[![Copper Factory MK3](https://gitlab.com/XenGi/satisfactory/-/raw/main/Copper%20Factory%20MK3.drawio.svg)](https://app.diagrams.net/#AXenGi%2Fsatisfactory%2Fmain%2FCopper%20Factory%20MK3.drawio.svg)

## Iron Factory

### MK2

- In: 3x120 Iron Ore
- Out:
  - 60 Iron Rod
  - 70 Iron Plate
  - 120 Screws
  - 10 Reinforced Iron Plate
  - 4 Rotor

[![Iron Factory MK2](https://gitlab.com/XenGi/satisfactory/-/raw/main/Iron%20Factory%20MK2.drawio.svg)](https://app.diagrams.net/#AXenGi%2Fsatisfactory%2Fmain%2FIron%20Factory%20MK2.drawio.svg)

### MK3 (WIP)

- In: 2x270 Iron Ore
- Out:
  - ? Iron Rod
  - ? Iron Plate
  - ? Screws
  - ? Reinforced Iron Plate
  - ? Rotor

[![Iron Factory MK3](https://gitlab.com/XenGi/satisfactory/-/raw/main/Iron%20Factory%20MK3.drawio.svg)](https://app.diagrams.net/#AXenGi%2Fsatisfactory%2Fmain%2FIron%20Factory%20MK3.drawio.svg)

## Steel Factory

### MK3

- In:
  - 270 Iron Ore
  - 270 Coal
- Out:
  - ? Steel Beam
  - ? Steel Pipe

[![Steel Factory MK3](https://gitlab.com/XenGi/satisfactory/-/raw/main/Steel%20Factory%20MK3.drawio.svg)](https://app.diagrams.net/#AXenGi%2Fsatisfactory%2Fmain%2FSteel%20Factory%20MK3.drawio.svg)
